; ModuleID = 'sample.cpp'
source_filename = "sample.cpp"
target datalayout = "e-m:e-p270:32:32-p271:32:32-p272:64:64-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-pc-linux-gnu"

; Function Attrs: noinline norecurse nounwind optnone mustprogress
define dso_local i32 @main(i32 %argc, i8** %argv) #0 {
entry:
  %retval = alloca i32, align 4
  %argc.addr = alloca i32, align 4
  %argv.addr = alloca i8**, align 8
  %a = alloca i16, align 2
  %b = alloca i16, align 2
  %c = alloca i16, align 2
  %d = alloca i16, align 2
  %e = alloca i16, align 2
  %f = alloca i16, align 2
  %sum = alloca i16, align 2
  %subp = alloca i16, align 2
  %subn = alloca i16, align 2
  %mulp = alloca i16, align 2
  %mul = alloca i16, align 2
  %muln = alloca i16, align 2
  %udiv = alloca i16, align 2
  %sdiv = alloca i16, align 2
  %land = alloca i16, align 2
  %lor = alloca i16, align 2
  %lxor = alloca i16, align 2
  %lnot = alloca i16, align 2
  %band = alloca i16, align 2
  %bor = alloca i16, align 2
  %bxor = alloca i16, align 2
  %bnot = alloca i16, align 2

  store i32 0, i32* %retval, align 4
  store i32 %argc, i32* %argc.addr, align 4
  store i8** %argv, i8*** %argv.addr, align 8
  store i16 20, i16* %a, align 2
  store i16 5, i16* %b, align 2
  store i16 -2, i16* %c, align 2
  store i16 -8, i16* %d, align 2
  store i16 20, i16* %e, align 2
  store i16 5, i16* %f, align 2

; ADD
  %0 = load i16, i16* %a, align 2
  %1 = load i16, i16* %b, align 2
  %add = add nsw i16 %0, %1
  store i16 %add, i16* %sum, align 2

  %2 = load i16, i16* %a, align 2
  %3 = load i16, i16* %b, align 2
  %sub = sub nsw i16 %2, %3
  store i16 %sub, i16* %subp, align 2

  %4 = load i16, i16* %a, align 2
  %5 = load i16, i16* %c, align 2
  %sub2 = sub nsw i16 %4, %5
  store i16 %sub2, i16* %subn, align 2


  %6 = load i16, i16* %a, align 2
  %7 = load i16, i16* %b, align 2
  %mul12 = mul nsw i16 %6, %7
  store i16 %mul12, i16* %mulp, align 2

  %8 = load i16, i16* %a, align 2
  %9 = load i16, i16* %c, align 2
  %mul16 = mul nsw i16 %8, %9
  store i16 %mul16, i16* %mul, align 2

  %10 = load i16, i16* %c, align 2
  %11 = load i16, i16* %d, align 2
  %mul20 = mul nsw i16 %10, %11
  store i16 %mul20, i16* %muln, align 2

  %12 = load i16, i16* %e, align 2
  %13 = load i16, i16* %f, align 2
  %div = sdiv i16 %12, %13
  store i16 %div, i16* %udiv, align 2

  %14 = load i16, i16* %a, align 2
  %15 = load i16, i16* %b, align 2
  %div27 = sdiv i16 %14, %15
  store i16 %div27, i16* %sdiv, align 2

  store i16 15, i16* %a, align 2
  store i16 6, i16* %b, align 2
  %16 = load i16, i16* %a, align 2
  %tobool = icmp ne i16 %16, 0
  br i1 %tobool, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %entry
  %17 = load i16, i16* %b, align 2
  %tobool29 = icmp ne i16 %17, 0
  br label %land.end

land.end:                                         ; preds = %land.rhs, %entry
  %18 = phi i1 [ false, %entry ], [ %tobool29, %land.rhs ]
  %conv30 = zext i1 %18 to i16
  store i16 %conv30, i16* %land, align 2
  %19 = load i16, i16* %a, align 2
  %tobool31 = icmp ne i16 %19, 0
  br i1 %tobool31, label %lor.end, label %lor.rhs

lor.rhs:                                          ; preds = %land.end
  %20 = load i16, i16* %b, align 2
  %tobool32 = icmp ne i16 %20, 0
  br label %lor.end

lor.end:                                          ; preds = %lor.rhs, %land.end
  %21 = phi i1 [ true, %land.end ], [ %tobool32, %lor.rhs ]
  %conv33 = zext i1 %21 to i16
  store i16 %conv33, i16* %lor, align 2

  %22 = load i16, i16* %a, align 2
  %23 = load i16, i16* %b, align 2
  %cmp = icmp ne i16 %22, %23
  %conv36 = zext i1 %cmp to i16
  store i16 %conv36, i16* %lxor, align 2
  
  %24 = load i16, i16* %a, align 2
  %tobool37 = icmp ne i16 %24, 0
  %lnot38 = xor i1 %tobool37, true
  %conv39 = zext i1 %lnot38 to i16
  store i16 %conv39, i16* %lnot, align 2

  %25 = load i16, i16* %a, align 2
  %26 = load i16, i16* %b, align 2
  %and = and i16 %25, %26
  store i16 %and, i16* %band, align 2
  
  %27 = load i16, i16* %a, align 2
  %28 = load i16, i16* %b, align 2
  %or = or i16 %27, %28
  store i16 %or, i16* %bor, align 2

  %29 = load i16, i16* %a, align 2
  %30 = load i16, i16* %b, align 2
  %xor = xor i16 %29, %30
  store i16 %xor, i16* %bxor, align 2

  %31 = load i16, i16* %a, align 2
  %neg = xor i16 %31, -1
  store i16 %neg, i16* %bnot, align 2

  ret i32 0
}

attributes #0 = { noinline norecurse nounwind optnone mustprogress "frame-pointer"="none" "min-legal-vector-width"="0" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-features"="+cx8,+mmx,+sse,+sse2,+x87" }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"Debian clang version 13.0.0-++20210616115150+3f18fc5ece72-1~exp1~20210616095941.1567"}
