int main(int argc, char ** argv){

short int a, b, c, d;
unsigned short int e, f;

/* arithmetic */

a = 20, b = 5, c = -2, d = -8;
e = 20, f = 5;

short int  sum, subp, subn, mulp, mul, muln, udiv, sdiv;

sum = a + b; // ADD
subp = a - b; // SUB
subn = a - c; // SUB -
mulp = a * b;// MUL ++
mul = a * c; // MUL +-
muln = c * d; // MUL --
udiv = e / f; // UDIV
sdiv = a / b; // SDIV

/* logic */

a = 15, b = 6;

short int land, lor, lxor, lnot, band, bor, bxor, bnot;

land = a && b;
lor = a || b;
lxor = a != b;
lnot = !a;

band = a & b;
bor = a | b;
bxor = a ^ b;
bnot = ~a;

return 0;
}
