; ModuleID = 'sample.cpp'
source_filename = "sample.cpp"
target datalayout = "e-m:e-p270:32:32-p271:32:32-p272:64:64-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-pc-linux-gnu"

; Function Attrs: noinline norecurse nounwind optnone mustprogress
define dso_local i32 @main(i32 %argc, i8** %argv) #0 {
entry:
  %retval = alloca i32, align 4
  %argc.addr = alloca i32, align 4
  %argv.addr = alloca i8**, align 8
  %a = alloca i16, align 2
  %b = alloca i16, align 2
  %c = alloca i16, align 2
  %d = alloca i16, align 2
  %e = alloca i16, align 2
  %f = alloca i16, align 2
  %sum = alloca i16, align 2
  %subp = alloca i16, align 2
  %subn = alloca i16, align 2
  %mulp = alloca i16, align 2
  %mul = alloca i16, align 2
  %muln = alloca i16, align 2
  %udiv = alloca i16, align 2
  %sdiv = alloca i16, align 2
  %land = alloca i16, align 2
  %lor = alloca i16, align 2
  %lxor = alloca i16, align 2
  %lnot = alloca i16, align 2
  %band = alloca i16, align 2
  %bor = alloca i16, align 2
  %bxor = alloca i16, align 2
  %bnot = alloca i16, align 2
  store i32 0, i32* %retval, align 4
  store i32 %argc, i32* %argc.addr, align 4
  store i8** %argv, i8*** %argv.addr, align 8
  store i16 20, i16* %a, align 2
  store i16 5, i16* %b, align 2
  store i16 -2, i16* %c, align 2
  store i16 -8, i16* %d, align 2
  store i16 20, i16* %e, align 2
  store i16 5, i16* %f, align 2
  %0 = load i16, i16* %a, align 2
  %conv = sext i16 %0 to i32
  %1 = load i16, i16* %b, align 2
  %conv1 = sext i16 %1 to i32
  %add = add nsw i32 %conv, %conv1
  %conv2 = trunc i32 %add to i16
  store i16 %conv2, i16* %sum, align 2
  %2 = load i16, i16* %a, align 2
  %conv3 = sext i16 %2 to i32
  %3 = load i16, i16* %b, align 2
  %conv4 = sext i16 %3 to i32
  %sub = sub nsw i32 %conv3, %conv4
  %conv5 = trunc i32 %sub to i16
  store i16 %conv5, i16* %subp, align 2
  %4 = load i16, i16* %a, align 2
  %conv6 = sext i16 %4 to i32
  %5 = load i16, i16* %c, align 2
  %conv7 = sext i16 %5 to i32
  %sub8 = sub nsw i32 %conv6, %conv7
  %conv9 = trunc i32 %sub8 to i16
  store i16 %conv9, i16* %subn, align 2
  %6 = load i16, i16* %a, align 2
  %conv10 = sext i16 %6 to i32
  %7 = load i16, i16* %b, align 2
  %conv11 = sext i16 %7 to i32
  %mul12 = mul nsw i32 %conv10, %conv11
  %conv13 = trunc i32 %mul12 to i16
  store i16 %conv13, i16* %mulp, align 2
  %8 = load i16, i16* %a, align 2
  %conv14 = sext i16 %8 to i32
  %9 = load i16, i16* %c, align 2
  %conv15 = sext i16 %9 to i32
  %mul16 = mul nsw i32 %conv14, %conv15
  %conv17 = trunc i32 %mul16 to i16
  store i16 %conv17, i16* %mul, align 2
  %10 = load i16, i16* %c, align 2
  %conv18 = sext i16 %10 to i32
  %11 = load i16, i16* %d, align 2
  %conv19 = sext i16 %11 to i32
  %mul20 = mul nsw i32 %conv18, %conv19
  %conv21 = trunc i32 %mul20 to i16
  store i16 %conv21, i16* %muln, align 2
  %12 = load i16, i16* %e, align 2
  %conv22 = zext i16 %12 to i32
  %13 = load i16, i16* %f, align 2
  %conv23 = zext i16 %13 to i32
  %div = sdiv i32 %conv22, %conv23
  %conv24 = trunc i32 %div to i16
  store i16 %conv24, i16* %udiv, align 2
  %14 = load i16, i16* %a, align 2
  %conv25 = sext i16 %14 to i32
  %15 = load i16, i16* %b, align 2
  %conv26 = sext i16 %15 to i32
  %div27 = sdiv i32 %conv25, %conv26
  %conv28 = trunc i32 %div27 to i16
  store i16 %conv28, i16* %sdiv, align 2
  store i16 15, i16* %a, align 2
  store i16 6, i16* %b, align 2
  %16 = load i16, i16* %a, align 2
  %tobool = icmp ne i16 %16, 0
  br i1 %tobool, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %entry
  %17 = load i16, i16* %b, align 2
  %tobool29 = icmp ne i16 %17, 0
  br label %land.end

land.end:                                         ; preds = %land.rhs, %entry
  %18 = phi i1 [ false, %entry ], [ %tobool29, %land.rhs ]
  %conv30 = zext i1 %18 to i16
  store i16 %conv30, i16* %land, align 2
  %19 = load i16, i16* %a, align 2
  %tobool31 = icmp ne i16 %19, 0
  br i1 %tobool31, label %lor.end, label %lor.rhs

lor.rhs:                                          ; preds = %land.end
  %20 = load i16, i16* %b, align 2
  %tobool32 = icmp ne i16 %20, 0
  br label %lor.end

lor.end:                                          ; preds = %lor.rhs, %land.end
  %21 = phi i1 [ true, %land.end ], [ %tobool32, %lor.rhs ]
  %conv33 = zext i1 %21 to i16
  store i16 %conv33, i16* %lor, align 2
  %22 = load i16, i16* %a, align 2
  %conv34 = sext i16 %22 to i32
  %23 = load i16, i16* %b, align 2
  %conv35 = sext i16 %23 to i32
  %cmp = icmp ne i32 %conv34, %conv35
  %conv36 = zext i1 %cmp to i16
  store i16 %conv36, i16* %lxor, align 2
  %24 = load i16, i16* %a, align 2
  %tobool37 = icmp ne i16 %24, 0
  %lnot38 = xor i1 %tobool37, true
  %conv39 = zext i1 %lnot38 to i16
  store i16 %conv39, i16* %lnot, align 2
  %25 = load i16, i16* %a, align 2
  %conv40 = sext i16 %25 to i32
  %26 = load i16, i16* %b, align 2
  %conv41 = sext i16 %26 to i32
  %and = and i32 %conv40, %conv41
  %conv42 = trunc i32 %and to i16
  store i16 %conv42, i16* %band, align 2
  %27 = load i16, i16* %a, align 2
  %conv43 = sext i16 %27 to i32
  %28 = load i16, i16* %b, align 2
  %conv44 = sext i16 %28 to i32
  %or = or i32 %conv43, %conv44
  %conv45 = trunc i32 %or to i16
  store i16 %conv45, i16* %bor, align 2
  %29 = load i16, i16* %a, align 2
  %conv46 = sext i16 %29 to i32
  %30 = load i16, i16* %b, align 2
  %conv47 = sext i16 %30 to i32
  %xor = xor i32 %conv46, %conv47
  %conv48 = trunc i32 %xor to i16
  store i16 %conv48, i16* %bxor, align 2
  %31 = load i16, i16* %a, align 2
  %conv49 = sext i16 %31 to i32
  %neg = xor i32 %conv49, -1
  %conv50 = trunc i32 %neg to i16
  store i16 %conv50, i16* %bnot, align 2
  ret i32 0
}

attributes #0 = { noinline norecurse nounwind optnone mustprogress "frame-pointer"="none" "min-legal-vector-width"="0" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-features"="+cx8,+mmx,+sse,+sse2,+x87" }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"Debian clang version 13.0.0-++20210616115150+3f18fc5ece72-1~exp1~20210616095941.1567"}
