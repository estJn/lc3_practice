; ModuleID = 'sample.cpp'
source_filename = "sample.cpp"
target datalayout = "e-m:e-p270:32:32-p271:32:32-p272:64:64-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-pc-linux-gnu"

; Function Attrs: noinline norecurse nounwind optnone uwtable mustprogress
define dso_local i32 @main(i32 %0, i8** %1) #0 {
  %3 = alloca i32, align 4
  %4 = alloca i32, align 4
  %5 = alloca i8**, align 8
  %6 = alloca i16, align 2
  %7 = alloca i16, align 2
  %8 = alloca i16, align 2
  %9 = alloca i16, align 2
  %10 = alloca i16, align 2
  %11 = alloca i16, align 2
  %12 = alloca i16, align 2
  %13 = alloca i16, align 2
  %14 = alloca i16, align 2
  %15 = alloca i16, align 2
  %16 = alloca i16, align 2
  %17 = alloca i16, align 2
  %18 = alloca i16, align 2
  %19 = alloca i16, align 2
  %20 = alloca i16, align 2
  %21 = alloca i16, align 2
  %22 = alloca i16, align 2
  %23 = alloca i16, align 2
  %24 = alloca i16, align 2
  %25 = alloca i16, align 2
  %26 = alloca i16, align 2
  %27 = alloca i16, align 2
  store i32 0, i32* %3, align 4
  store i32 %0, i32* %4, align 4
  store i8** %1, i8*** %5, align 8
  store i16 20, i16* %6, align 2
  store i16 5, i16* %7, align 2
  store i16 -2, i16* %8, align 2
  store i16 -8, i16* %9, align 2
  store i16 20, i16* %10, align 2
  store i16 5, i16* %11, align 2
  %28 = load i16, i16* %6, align 2
  %29 = sext i16 %28 to i32
  %30 = load i16, i16* %7, align 2
  %31 = sext i16 %30 to i32
  %32 = add nsw i32 %29, %31
  %33 = trunc i32 %32 to i16
  store i16 %33, i16* %12, align 2
  %34 = load i16, i16* %6, align 2
  %35 = sext i16 %34 to i32
  %36 = load i16, i16* %7, align 2
  %37 = sext i16 %36 to i32
  %38 = sub nsw i32 %35, %37
  %39 = trunc i32 %38 to i16
  store i16 %39, i16* %13, align 2
  %40 = load i16, i16* %6, align 2
  %41 = sext i16 %40 to i32
  %42 = load i16, i16* %8, align 2
  %43 = sext i16 %42 to i32
  %44 = sub nsw i32 %41, %43
  %45 = trunc i32 %44 to i16
  store i16 %45, i16* %14, align 2
  %46 = load i16, i16* %6, align 2
  %47 = sext i16 %46 to i32
  %48 = load i16, i16* %7, align 2
  %49 = sext i16 %48 to i32
  %50 = mul nsw i32 %47, %49
  %51 = trunc i32 %50 to i16
  store i16 %51, i16* %15, align 2
  %52 = load i16, i16* %6, align 2
  %53 = sext i16 %52 to i32
  %54 = load i16, i16* %8, align 2
  %55 = sext i16 %54 to i32
  %56 = mul nsw i32 %53, %55
  %57 = trunc i32 %56 to i16
  store i16 %57, i16* %16, align 2
  %58 = load i16, i16* %8, align 2
  %59 = sext i16 %58 to i32
  %60 = load i16, i16* %9, align 2
  %61 = sext i16 %60 to i32
  %62 = mul nsw i32 %59, %61
  %63 = trunc i32 %62 to i16
  store i16 %63, i16* %17, align 2
  %64 = load i16, i16* %10, align 2
  %65 = zext i16 %64 to i32
  %66 = load i16, i16* %11, align 2
  %67 = zext i16 %66 to i32
  %68 = sdiv i32 %65, %67
  %69 = trunc i32 %68 to i16
  store i16 %69, i16* %18, align 2
  %70 = load i16, i16* %6, align 2
  %71 = sext i16 %70 to i32
  %72 = load i16, i16* %7, align 2
  %73 = sext i16 %72 to i32
  %74 = sdiv i32 %71, %73
  %75 = trunc i32 %74 to i16
  store i16 %75, i16* %19, align 2
  store i16 15, i16* %6, align 2
  store i16 6, i16* %7, align 2
  %76 = load i16, i16* %6, align 2
  %77 = icmp ne i16 %76, 0
  br i1 %77, label %78, label %81

78:                                               ; preds = %2
  %79 = load i16, i16* %7, align 2
  %80 = icmp ne i16 %79, 0
  br label %81

81:                                               ; preds = %78, %2
  %82 = phi i1 [ false, %2 ], [ %80, %78 ]
  %83 = zext i1 %82 to i16
  store i16 %83, i16* %20, align 2
  %84 = load i16, i16* %6, align 2
  %85 = icmp ne i16 %84, 0
  br i1 %85, label %89, label %86

86:                                               ; preds = %81
  %87 = load i16, i16* %7, align 2
  %88 = icmp ne i16 %87, 0
  br label %89

89:                                               ; preds = %86, %81
  %90 = phi i1 [ true, %81 ], [ %88, %86 ]
  %91 = zext i1 %90 to i16
  store i16 %91, i16* %21, align 2
  %92 = load i16, i16* %6, align 2
  %93 = sext i16 %92 to i32
  %94 = load i16, i16* %7, align 2
  %95 = sext i16 %94 to i32
  %96 = icmp ne i32 %93, %95
  %97 = zext i1 %96 to i16
  store i16 %97, i16* %22, align 2
  %98 = load i16, i16* %6, align 2
  %99 = icmp ne i16 %98, 0
  %100 = xor i1 %99, true
  %101 = zext i1 %100 to i16
  store i16 %101, i16* %23, align 2
  %102 = load i16, i16* %6, align 2
  %103 = sext i16 %102 to i32
  %104 = load i16, i16* %7, align 2
  %105 = sext i16 %104 to i32
  %106 = and i32 %103, %105
  %107 = trunc i32 %106 to i16
  store i16 %107, i16* %24, align 2
  %108 = load i16, i16* %6, align 2
  %109 = sext i16 %108 to i32
  %110 = load i16, i16* %7, align 2
  %111 = sext i16 %110 to i32
  %112 = or i32 %109, %111
  %113 = trunc i32 %112 to i16
  store i16 %113, i16* %25, align 2
  %114 = load i16, i16* %6, align 2
  %115 = sext i16 %114 to i32
  %116 = load i16, i16* %7, align 2
  %117 = sext i16 %116 to i32
  %118 = xor i32 %115, %117
  %119 = trunc i32 %118 to i16
  store i16 %119, i16* %26, align 2
  %120 = load i16, i16* %6, align 2
  %121 = sext i16 %120 to i32
  %122 = xor i32 %121, -1
  %123 = trunc i32 %122 to i16
  store i16 %123, i16* %27, align 2
  ret i32 0
}

attributes #0 = { noinline norecurse nounwind optnone uwtable mustprogress "frame-pointer"="all" "min-legal-vector-width"="0" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+cx8,+fxsr,+mmx,+sse,+sse2,+x87" "tune-cpu"="generic" }

!llvm.module.flags = !{!0, !1, !2}
!llvm.ident = !{!3}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{i32 7, !"uwtable", i32 1}
!2 = !{i32 7, !"frame-pointer", i32 2}
!3 = !{!"Debian clang version 13.0.0-++20210616115150+3f18fc5ece72-1~exp1~20210616095941.1567"}
