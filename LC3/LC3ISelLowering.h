//===-- LC3ISelLowering.h - LC3 DAG Lowering Interface ------*- C++ -*-===//
//
// Part of the LLVM Project, under the Apache License v2.0 with LLVM Exceptions.
// See https://llvm.org/LICENSE.txt for license information.
// SPDX-License-Identifier: Apache-2.0 WITH LLVM-exception
//
//===----------------------------------------------------------------------===//
//
// This file defines the interfaces that LC3 uses to lower LLVM code into a
// selection DAG.
//
//===----------------------------------------------------------------------===//
  
#ifndef LC3ISELLOWERING_H
#define LC3ISELLOWERING_H


#include "LC3.h"
#include "llvm/CodeGen/CallingConvLower.h"
#include "llvm/CodeGen/SelectionDAG.h"
#include "llvm/CodeGen/TargetLowering.h"
  
namespace llvm {
    class LC3Subtarget;
    struct LC3RegisterInfo;
    namespace LC3ISD {
        enum NodeType : unsigned {
            FIRST_NUMBER = ISD::BUILTIN_OP_END,
            RET,
            NOT
        };
    } // namespace LCSISD

    class LC3TargetLowering : public TargetLowering {
        public:

        // - Insert Prologue & Epilogue code into the function.
        // 
        // push basePtr
        // basePtr <- stackPtr
        // stackPtr -= N    (where N is an immediate value of byte-size saved local use..)
        void emitPrologue(MachineFunction &MF, MachineBasicBlock &MBB) const override;

        // JMP / RET could handles
        // stackPtr <- basePtr
        // pop basePtr
        // ret
        void emitEpilogue(MachineFunction &MF, MachineBasicBlock &MBB) const override;
    };
}

#endif // LC3ISELLOWERING_H