//===-- RISCVRegisterInfo.h - LC3 Register Information Impl ---*- C++ -*-===//
//
// Part of the LLVM Project, under the Apache License v2.0 with LLVM Exceptions.
// See https://llvm.org/LICENSE.txt for license information.
// SPDX-License-Identifier: Apache-2.0 WITH LLVM-exception
//
//===----------------------------------------------------------------------===//
//
// This file contains the LC3 implementation of the TargetRegisterInfo class.
//
//===----------------------------------------------------------------------===//

#ifndef LLVM_LIB_TARGET_LC3_LC3REGISTERINFO_H
#define LLVM_LIB_TARGET_LC3_LC3REGISTERINFO_H

#include "llvm/CodeGen/TargetRegisterInfo.h"

#define GET_REGINFO_HEADER
#include "LC3GenRegisterInfo.inc"

namespace llvm {

    struct LC3RegisterInfo : public LC3GenRegisterInfo {

        LC3RegisterInfo();

        const MCPhysReg *getCalleeSavedRegs(const MachineFunction *MF) const override;
        
        const uint32_t *getCallPreservedMask(const MachineFunction &MF,
                                            CallingConv::ID) const override;

        BitVector getReservedRegs(const MachineFunction &MF) const override;

        const TargetRegisterClass * getPointerRegClass(const MachineFunction &MF, 
                                                        unsigned Kind = 0) const override;
        
        // "resolve a frame index operand of an instruction to reference the indicated base register plus offset instead"
        void resolveFrameIndex(MachineInstr &MI, unsiged BaseReg, int64_t Offset) const override;
        
        // "determined weather a given base register plus offset immediate is encodable to resolve a frame index"
        bool isFrameOffsetLegal(const MachineInstr *MI, usinged BaseReg, int64_t Offset) const override;
        
        // returns register used as a base for values allocated in the current stack frame (?)
        Register getFrameRegister(const MachineFunction &MF) const override;

        void eliminateFrameIndex(MachineBasicBlock::iterator MI, int SPAdj,
                                unsigned FIOperandNum,
                                RegScavenger *RS = nullptr) const override;

    }; // end struct LC3RegisterInfo

}// end namespace llvm

#endif