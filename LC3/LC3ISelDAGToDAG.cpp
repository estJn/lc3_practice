//===-- LC3ISelDAGToDAG.cpp - A dag to dag inst selector for LC3 ------===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
// * Implemented with SparcISelDAGToDAT.cpp
//===----------------------------------------------------------------------===//
//
// This file defines an instruction selector for the LC3 target.
//
//===----------------------------------------------------------------------===//

#include "LC3GenDAGIsel.inc"

#include "LC3TargetMachine.h"
#include "llvm/CodeGen/MachineRegisterInfo.h"
#include "llvm/CodeGen/SelectionDAGISel.h"
#include "llvm/IR/Intrinsics.h"
#include "llvm/Support/Debug.h"
#include "llvm/Support/ErrorHandling.h"
#include "llvm/Support/raw_ostream.h"

using namespace llvm;


//===----------------------------------------------------------------------===//
// Instruction Selector Implementation
//===----------------------------------------------------------------------===//

namespace {

    class LC3DAGToDAGISel : public SelectionDAGISel {
        /// Subtarget - Keep a pointer to the LC3 Subtarget around so that we can
        /// make the right decision when generating code for different targets.
        const LC3Subtarget &Subtarget = nullptr;

        public:
            explicit LC3DAGToDAGISel(): LC3DAGToDAGISel() {

            }

            void Select(SDNode *N) override;
            
            // Complex Pattern Selector
            bool SelectAddrRI(SDNode N, SDValue &Base, SDValue &Offset);
            bool SelectAddrI(SDNode N, SDValue &Offset);

            StringRef getPassName() const override {
                return "LC3 DAG->DAG Pattern Instruction Selection";
            }

        private:

       
    }; //class LC3DAGToDAGISel

} // namespace

bool LC3DAGToDAGISel::SelectAddrRI(SDValue Addr, SDValue &Base, SDValue &Offset) {
    if (FrameIndexSDNode *FIN = dyn_cast<FrameIndexSDNode>(Addr)) {
        Base = CurDAG->getTargetFrameIndex(
            FIN->getIndex(), TLI->getPointerTy(CurDAG->getDataLayout()));
        Offset = CurDAG->getTargetConstant(0, SDLoc(Addr), MVT::i16);
        return true;
    }
    if (Addr.getOpcode() == ISD::TargetExternalSymbol ||
        Addr.getOpcode() == ISD::TargetGlobalAddress ||
        Addr.getOpcode() == ISD::TargetGlobalTLSAddress)
        return false;  // direct calls.

    
    Base = Addr;
    Offset = CurDAG->getTargetConstant(0, SDLoc(Addr), MVT::i16);
    return true;
}
