//===-- LC3InstrInfo.td - Target Description for LC3 Target -----------===//
//
// Part of the LLVM Project, under the Apache License v2.0 with LLVM Exceptions.
// See https://llvm.org/LICENSE.txt for license information.
// SPDX-License-Identifier: Apache-2.0 WITH LLVM-exception
//
//===----------------------------------------------------------------------===//
//
// This file describes the LC3 instructions in TableGen format.
//
//===----------------------------------------------------------------------===//

include "LC3InstrFormats.td"

//===----------------------------------------------------------------------===//
// LC3 DAG Nodes 
//===----------------------------------------------------------------------===//

// <(outs), (ins), "assembly string", [list pattern]>

// OPERANDS
// outs     :   list of definitions or outputs
// ins      :   list of uses or inputs
// operand  :   RegistrClass (GenRegs, ProcRegs)
//          :   Immediate (i#imm)

// ASSEMBLY STRING
// Define Pattern, we see

// LIST PATTERN
// Matches Nodes in SelectionDAG
// Syntax   :   One pair of Parenthesis definions one note
//              Nodes has DAG operands with MVT type (i#, iPTR...)
//              Map DAG operands to MI operands


def SDT_LC3_Call    :   SDTypeProfile<0, -1, [SDTCisPtrTy<0>]>;
def SDT_LC3_Ret     :   SDTypeProfile<0, 1, [SDTCisInt<0>]>;

def RetFlag         :   SDNode<"LC3ISD::RET_FLAG", SDTNone, [SDNPHasChain, SDNPOptInGlue, SDNPVariadic]>;

// LLVM: MachineTypeValue.h // MCCodeEmiter
def LC3imm9 : Operand<i16>, ImmLeaf<i16, [{ return Imm >= 0 && Imm < 512; }]>;
def LC3imm6 : Operand<i16>, ImmLeaf<i16, [{ return Imm >= 0 && Imm < 64; }]>;
def LC3imm5 : Operand<i16>, ImmLeaf<i16, [{ return Imm >= 0 && Imm < 32; }]>;
def LC3imm3 : Operand<i16>, ImmLeaf<i16, [{ return Imm >= 0 && Imm < 8; }]>;

def br_cc   : Operand<i16>{ let PrintMethod = "printCondCodeOperand"; }
def addrRI  : ComplexPattern<iPTR, 2, "SelectAddrRI", [],[]>;
def addrR   : ComplexPattern<iPTR, 1, "SelectAddrI", [],[]>;
def addr    : ComplexPattern<iPTR, 1, "SelectAddr", [], []>;

/*
    
*/

// Address Operands
def LC3MEMrrAsmOperand  : AsmOperandClass {
    let Name = "MEMrr";
    let ParserMethod = "parseMemOperand";
}
def LC3MEMriAsmOperand  : AsmOperandClass {
    let Name = "MEMri";
    let ParserMethod = "parseMemOperand";
}

def MEMrr     : Operand<iPTR>{
    let PrintMethod = "printMemOperand";        // from LC3InstPrinter.cpp
    let EncoderMethod = "getMemEncoding";       // from LC3MCCodeEmitter.cpp
    let ParserMatchClass = LC3MEMrrAsmOperand;    // from LC3AsmParser.cpp
    let MIOperandInfo = (ops GenRegs, GenRegs);
}
def MEMri     : Operand<iPTR>{
    let PrintMethod = "printMemOperand";        // from LC3InstPrinter.cpp
    let EncoderMethod = "getMemEncoding";       // from LC3MCCodeEmitter.cpp
    let ParserMatchClass = LC3MEMriAsmOperand;    // from LC3AsmParser.cpp
    let MIOperandInfo = (ops GenRegs, i16imm);
}

def memsrc  : Operand<i16>{
    let PrintMethod = "printMemSrcOperand";
    let EncoderMethod = "getMemSrcValue";
    let MIOperandInfo = (ops GenRegs, i16imm);
}
 
//===----------------------------------------------------------------------===//
//    BRANCH
//===----------------------------------------------------------------------===//
let isBranch = 1, isTerminator = 1, Uses = [SR] in {
    // def BR  : LC3Inst<0b0000, (outs), (ins mem:$addr), 
    //                     ("BR $addr"), [(br bb:$addr)]>{

    //     bits<9> addr;
    //     let Inst{11-9} = 0b000; // == 0b111 (unconditional branch)
    //     let Inst{8-0} = addr;

    // }//Branch Unconditional

    def BRcc  : LC3Inst<0b0000, (outs), (ins MEMri:$addr br_cc:$cc)
                            ("BR $cc $addr"), [(brcc bb:$addr, LC3imm3:$cc)]>{

        bits<9> addr;
        bits<3> cc;
        let Inst{11-9} = cc; // == n z p condition
        let Inst{8-0} = addr;
    }
}
//===----------------------------------------------------------------------===//
//    JMP & RET : Return from SubRoutine
//===----------------------------------------------------------------------===//
def JMP : LC3Inst<0b1100, (outs), (ins GenRegs:$baseR)
                        ("JMP $baseR"), []>{

    bits<3> baseR;
    let Inst{11-9} = 0b000;
    let Inst{8-6} = baseR;
    let Inst{5-0} = 0b000000;
}// JMP R# : PC <- R#

// variable_ops : it takes a varaible number of operands
// In this case, variable_ops <- 0 | 1
def RET : LC3Inst<0b1100, (outs), (ins)
                        ("RET"), []>{
    let Inst{11-0} = 0b000111000000;
}// RET : PC <- R7

//===----------------------------------------------------------------------===//
//    JSR & JSRR :  Jump to SubRoutine
//                  R7 = PC, Then GOTO... the label.
//===----------------------------------------------------------------------===//

def JSR : LC3Inst<0b0100, (outs), (ins i11imm:$addr)
                        ("JSR $addr"), []>{

    bits<11> addr;
    let Inst{11} = 1;
    let Inst{10-0} = addr;
}// JSR addr : PC = PC + sext(addr)

def JSRR : LC3Inst<0b0100, (outs), (ins GenRegs:$baseR)
                        ("JSRR $baseR"), []>{

    bits<3> baseR;
    let Inst{11-9} = 0b000;
    let Inst{8-6} = baseR;
    let Inst{5-0} = 0b000000;
}// JSRR Rn : PC = Rn


//===----------------------------------------------------------------------===//
//    LOAD & STORE
//===----------------------------------------------------------------------===//

def LD  :   LC3Inst<0b0010, (outs GenRegs:$dst), (ints LC3imm9:$addr),
                    "LD $dst, $addr",
                    [(set i16:$dst, (sextloadi16 mem:$addr))]
                    >{
    bits<3> dst;
    bits<9> addr;
    let Inst{11-9} = dst;
    let Inst{8-0} = addr;
}

def LDI :   LC3Inst<>

multiclass Load<>{
    def #NAME#  : LC3Inst<0b0010, (outs GenRegs:$dst), (ins MEMrr:$addr),
                            "LD\t$dst, $addr", // !strconcat(opstr, "\t$dst, $addr"),
                            [(set i16:$dst, (sextloadi16 addr:$addr))]
                            >{}
    def I       : LC3Inst<0b1010, (outs GenRegs:$dst), (ins MEMri:$imm9),
                            "LDI\t$dst, $imm9",
                            [(set i16:$dst, (sextloadi16 ))]
                            >
}



def STORE   :   LC3Inst<0b0011, (outs GenReg:$src), (ins simm9:$addr),
                        "ST $src, $addr",
                        [(store i16:$src, mem$addr)]>{
    bits<3> src;
    bits<9> addr;
    let Inst{11-9} = src;
    let Inst{8-0} = addr;
}

// Arithmetic Logic Unit
let Defs = [SR] in {
    multiclass OpALU<bits<4> opcode, string opstr, SDNode opnode>{

        
        def rr :    ALUInst<
                            opcode,
                            (outs GenRegs:$dr), 
                            (ins GenRegs:$sr1, GenRegs:sr2), 
                            !strconcat(opstr, " $dr, $sr1, $sr2"),
                            [
                                (set i16:$dr, (opnode i16:$sr1, i16:$sr2)),
                                (implicit SR) // Modify (Program) Status Register
                            ]>{
            bits<3> dr;
            bits<3> sr1;
            bits<3> sr2;
            let Inst{11-9} = dr;
            let Inst{8-6} = sr1;
            let Inst{5} = 0;
            let Inst{4-3} = 0b00;
            let Inst{2-0} = sr2;
        }
        
        def ri :    ALUInst<
                            0b0001,
                            (outs GenRegs:$dr), 
                            (ins GenRegs:$sr, i5imm:$imm5), 
                            !strconcat(opstr, " $dr, $sr, $imm5"),
                            [
                                (set i16:$dr, (opnode i16:$sr1, LC3imm5:$imm5)),
                                (implicit SR)
                            ]>{
            bits<3> dr;
            bits<3> sr1;
            bits<3> sr2;
            let Inst{11-9} = dr;
            let Inst{8-6} = sr1;
            let Inst{5} = 1;
            let Inst{4-0} = sr2;
        }
    }
}

defm ADD : OpALU<0b0001, "ADD", add>;
defm AND : OpALU<0b0101, "AND", and>;
