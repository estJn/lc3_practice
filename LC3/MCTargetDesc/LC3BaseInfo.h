//===-- LC3BaseInfo.h - Top level definitions for LC3 -------- --*- C++ -*-===//
//
// Part of the LLVM Project, under the Apache License v2.0 with LLVM Exceptions.
// See https://llvm.org/LICENSE.txt for license information.
// SPDX-License-Identifier: Apache-2.0 WITH LLVM-exception
//
//===----------------------------------------------------------------------===//
//
// This file contains small standalone helper functions and enum definitions for
// the LC3 target useful for the compiler back-end and the MC libraries.
// As such, it deliberately does not include references to LLVM core
// code gen types, passes, etc..
//
//===----------------------------------------------------------------------===//

#ifndef LLVM_LIB_TARGET_LC3_MCTARGETDESC_LC3BASEINFO_H
#define LLVM_LIB_TARGET_LC3_MCTARGETDESC_LC3BASEINFO_H

#include "LC3MCTargetDesc.h"
#include "llvm/MC/MCInstrDesc.h"
#include "llvm/Support/DataTypes.h"
#include "llvm/Support/ErrorHandling.h"

namespace llvm {

    namespace LC3 {
        // Enums for memory operand decoding.  Each memory operand is represented with
        // a 5 operand sequence in the form:
        //   [BaseReg, ScaleAmt, IndexReg, Disp, Segment]
        // These enums help decode this.
        enum {
            AddrBaseReg = 0,
            AddrScaleAmt = 1,
            AddrIndexReg = 2,
            AddrDisp = 3,

            /// AddrSegmentReg - The operand # of the segment in the memory operand.
            AddrSegmentReg = 4,

            /// AddrNumOperands - Total number of operands in a memory reference.
            AddrNumOperands = 5
        };

        enum OperandType : unsigned {
            OPERAND_ROUNDING_CONTROL = MCOI::OPERAND_FIRST_TARGET,
            OPERAND_COND_CODE,
        };

        // LC3 specific condition code.
        enum CondCode {
            COND_U  = 0,    // Unconditional                    000
            COND_N  = 4,    // negative                         100
            COND_Z  = 2,    // zero                             010
            COND_P  = 1,    // positive                         001
            COND_NZ = 6,    // negative or zero (NOT positive)  110
            COND_NP = 5,    // negative OR positive (NOT zero)  101
            COND_ZP = 3,    // zero OR positive (NOT negative)  011
            
        };
    } // end namespace LC3;


} // end namespace llvm;

#endif