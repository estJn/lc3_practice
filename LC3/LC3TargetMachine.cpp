
 //===----------------------------------------------------------------------===//
 //
 // Implements LC3 target spec.
 //
 //===----------------------------------------------------------------------===//

 #include "LC3TargetMachine.h"
 #include "MCTargetDesc/LC3BaseInfo.h"
 #include "LC3.h"
 #include "LC3TargetObjectFile.h"
 #include "LC3TargetTransformInfo.h"
 #include "TargetInfo/LC3TargetInfo.h"
 #include "llvm/ADT/STLExtras.h"
 #include "llvm/Analysis/TargetTransformInfo.h"
 #include "llvm/CodeGen/GlobalISel/IRTranslator.h"
 #include "llvm/CodeGen/GlobalISel/InstructionSelect.h"
 #include "llvm/CodeGen/GlobalISel/Legalizer.h"
 #include "llvm/CodeGen/GlobalISel/RegBankSelect.h"
 #include "llvm/CodeGen/Passes.h"
 #include "llvm/CodeGen/TargetLoweringObjectFileImpl.h"
 #include "llvm/CodeGen/TargetPassConfig.h"
 #include "llvm/IR/LegacyPassManager.h"
 #include "llvm/InitializePasses.h"
 #include "llvm/Support/FormattedStream.h"
 #include "llvm/Support/TargetRegistry.h"
 #include "llvm/Target/TargetOptions.h"
 using namespace llvm;

 extern "C" LLVM_EXTERNAL_VISIBILITY void LLVMInitializeLC3Target() {
   RegisterTargetMachine<LC3TargetMachine> X(getTheLC3Target());
   RegisterTargetMachine<LC3TargetMachine> Y(getTheLC332Target());
   auto *PR = PassRegistry::getPassRegistry();
   initializeGlobalISel(*PR);
   initializeLC3MergeBaseOffsetOptPass(*PR);
   initializeLC3ExpandPseudoPass(*PR);
   initializeLC3InsertVSETVLIPass(*PR);
 }

 static StringRef computeDataLayout(const Triple &TT) {

   /**
   * # (n-bit) : # (alignment bit) : # (alwyas try to align)
   * eg) -v64:32:64-v128:32:128 (two sized vector : aligns 32-bit : always try to give them natural alignment )
   *
   * e/E       (Little / Big Endianness)
   * -m:e
   * -p:#:#    (Size of Pointer #-bit : aligned to #-bit)
   * [-Fi#]    (#-bit Function Pointer)
   * -i#:#     (#-bit Integer : aligned to #-bit)
   * [-f#:#:#] (#-bit Floating point : aligned to # bit : try to give align #-bit)
   * [-v#:]    (vector : APCS ABI align bit : trying align bit)
   * -n        (Integer Register)
   * -S        (Stack)
   **/

   if(TT.isArch16Bit())
    return "e-m:e-p:16:16-Fi16-i16:16-v16:16-n16-S16";

   // if (TT.isArch64Bit())
   //   return "e-m:e-p:64:64-i64:64-i128:128-n64-S128";
   // assert(TT.isArch32Bit() && "only RV32 and RV64 are currently supported");
   return "e-m:e-p:32:32-i64:64-n32-S128";
 }

 static Reloc::Model getEffectiveRelocModel(const Triple &TT, Optional<Reloc::Model> RM) {
   if (!RM.hasValue())
     return Reloc::Static;
   return *RM;
 }

 LC3TargetMachine::LC3TargetMachine(const Target &T, const Triple &TT,
                                        StringRef CPU, StringRef FS,
                                        const TargetOptions &Options,
                                        Optional<Reloc::Model> RM,
                                        Optional<CodeModel::Model> CM,
                                        CodeGenOpt::Level OL, bool JIT)
     : LLVMTargetMachine(T, computeDataLayout(TT), TT, CPU, FS, Options,
                         getEffectiveRelocModel(TT, RM),
                         getEffectiveCodeModel(CM, CodeModel::Small), OL),
       TLOF(std::make_unique<LC3ELFTargetObjectFile>()) {
   initAsmInfo();

   // // RISC-V supports the MachineOutliner.
   // setMachineOutliner(true);
 }

 const LC3Subtarget *
 LC3TargetMachine::getSubtargetImpl(const Function &F) const {
   Attribute CPUAttr = F.getFnAttribute("target-cpu");
   Attribute TuneAttr = F.getFnAttribute("tune-cpu");
   Attribute FSAttr = F.getFnAttribute("target-features");

   std::string CPU =
       CPUAttr.isValid() ? CPUAttr.getValueAsString().str() : TargetCPU;
   std::string TuneCPU =
       TuneAttr.isValid() ? TuneAttr.getValueAsString().str() : CPU;
   std::string FS =
       FSAttr.isValid() ? FSAttr.getValueAsString().str() : TargetFS;
   std::string Key = CPU + TuneCPU + FS;
   auto &I = SubtargetMap[Key];
   if (!I) {
     // This needs to be done before we create a new subtarget since any
     // creation will depend on the TM and the code generation flags on the
     // function that reside in TargetOptions.
     resetTargetOptions(F);
     auto ABIName = Options.MCOptions.getABIName();
     if (const MDString *ModuleTargetABI = dyn_cast_or_null<MDString>(
             F.getParent()->getModuleFlag("target-abi"))) {
       auto TargetABI = LC3ABI::getTargetABI(ABIName);
       if (TargetABI != LC3ABI::ABI_Unknown &&
           ModuleTargetABI->getString() != ABIName) {
         report_fatal_error("-target-abi option != target-abi module flag");
       }
       ABIName = ModuleTargetABI->getString();
     }
     I = std::make_unique<LC3Subtarget>(TargetTriple, CPU, TuneCPU, FS, ABIName, *this);
   }
   return I.get();
 } // getSubTargetImpl

 TargetTransformInfo
 LC3TargetMachine::getTargetTransformInfo(const Function &F) {
   return TargetTransformInfo(LC3TTIImpl(this, F));
 }

 // A RISC-V hart has a single byte-addressable address space of 2^XLEN bytes
 // for all memory accesses, so it is reasonable to assume that an
 // implementation has no-op address space casts. If an implementation makes a
 // change to this, they can override it here.
 bool LC3TargetMachine::isNoopAddrSpaceCast(unsigned SrcAS,
                                              unsigned DstAS) const {
   return true;
 }

 namespace {
 class LC3PassConfig : public TargetPassConfig {
 public:
   LC3PassConfig(LC3TargetMachine &TM, PassManagerBase &PM)
       : TargetPassConfig(TM, PM) {}

   LC3TargetMachine &getLC3TargetMachine() const {
     return getTM<LC3TargetMachine>();
   }

// https://llvm.org/doxygen/classllvm_1_1TargetPassConfig.html#a835d2863dbd2cfd8c184a6a94923b61f
   void addIRPasses() override;
   bool addInstSelector() override;
   bool addIRTranslator() override;
   bool addLegalizeMachineIR() override;
   bool addRegBankSelect() override;
   bool addGlobalInstructionSelect() override;
   // CodeGen/TargetPassConfig
   void addPreEmitPass() override;
   void addPreEmitPass2() override;
   void addPreSched2() override;
   void addPreRegAlloc() override;
 };
 } // namespace

 TargetPassConfig *LC3TargetMachine::createPassConfig(PassManagerBase &PM) {
   return new LC3PassConfig(*this, PM);
 }

 void LC3PassConfig::addIRPasses() {
   addPass(createAtomicExpandPass());
   TargetPassConfig::addIRPasses();
 }

 bool LC3PassConfig::addInstSelector() {
   addPass(createLC3ISelDag(getLC3TargetMachine()));

   return false;
 }

 bool LC3PassConfig::addIRTranslator() {
   addPass(new IRTranslator(getOptLevel()));
   return false;
 }

// This method should install a legalize pass,
// which converts the instruction sequence into one that can be selected by the target.
 bool LC3PassConfig::addLegalizeMachineIR() {
   addPass(new Legalizer());
   return false;
 }

 bool LC3PassConfig::addRegBankSelect() {
   addPass(new RegBankSelect());
   return false;
 }

 bool LC3PassConfig::addGlobalInstructionSelect() {
   addPass(new InstructionSelect(getOptLevel()));
   return false;
 }

 void LC3PassConfig::addPreSched2() {}

 void LC3PassConfig::addPreEmitPass() { addPass(&BranchRelaxationPassID); }

 void LC3PassConfig::addPreEmitPass2() {
   addPass(createLC3ExpandPseudoPass());
   // Schedule the expansion of AMOs at the last possible moment, avoiding the
   // possibility for other passes to break the requirements for forward
   // progress in the LR/SC block.
   addPass(createLC3ExpandAtomicPseudoPass());
 }

 void LC3PassConfig::addPreRegAlloc() {
   if (TM->getOptLevel() != CodeGenOpt::None)
     addPass(createLC3MergeBaseOffsetOptPass());
   addPass(createLC3InsertVSETVLIPass());
 }
