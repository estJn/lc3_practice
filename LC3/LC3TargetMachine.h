
 //===----------------------------------------------------------------------===//
 //
 // LC3 specific subclass of TargetMachine.
 //
 //===----------------------------------------------------------------------===//

 #ifndef LC3TARGETMACHINE_H
 #define LC3TARGETMACHINE_H

 // #include "MCTargetDesc/LC3MCTargetDesc.h"
 #include "LC3Subtarget.h"
 #include "LC3InstrInfo.h"
 #include "llvm/CodeGen/SelectionDAGTargetInfo.h"
 #include "llvm/IR/DataLayout.h"
 #include "llvm/Target/TargetMachine.h"

 namespace llvm {
 class LC3TargetMachine : public LLVMTargetMachine {
   
   std::unique_ptr<TargetLoweringObjectFile> TLOF;
   LC3Subtarget Subtarget;
   LC3InstrInfo InstrInfo;

   public:
     LC3TargetMachine(const Target &T, const Triple &TT, StringRef CPU,
                        StringRef FS, const TargetOptions &Options,
                        Optional<Reloc::Model> RM, Optional<CodeModel::Model> CM,
                        CodeGenOpt::Level OL, bool JIT);

     const LC3Subtarget *getSubtargetImpl(const Function &F) const override;
     // DO NOT IMPLEMENT: There is no such thing as a valid default subtarget,
     // subtargets are per-function entities based on the target-specific
     // attributes of each function.
     const LC3Subtarget *getSubtargetImpl() const = delete;

     // Pass Pipeline Configuration
     TargetPassConfig *createPassConfig(PassManagerBase &PM) override;
     TargetLoweringObjectFile *getObjFileLowering() const override {
       return TLOF.get();
     }

     TargetTransformInfo getTargetTransformInfo(const Function &F) override;

     virtual bool isNoopAddrSpaceCast(unsigned SrcAS,
                                      unsigned DstAS) const override;
 };
 } // namespace llvm

 #endif
