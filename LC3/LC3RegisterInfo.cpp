/===-- LC3RegisterInfo.cpp - SPARC Register Information ----------------===//
//
// Part of the LLVM Project, under the Apache License v2.0 with LLVM Exceptions.
// See https://llvm.org/LICENSE.txt for license information.
// SPDX-License-Identifier: Apache-2.0 WITH LLVM-exception
//
//===----------------------------------------------------------------------===//
//
// This file contains the SPARC implementation of the TargetRegisterInfo class.
//
//===----------------------------------------------------------------------===//

#include "LC3RegisterInfo.h"
#include "LC3.h"
#include "LC3MachineFunctionInfo.h"
#include "LC3Subtarget.h"
#include "llvm/ADT/BitVector.h"
#include "llvm/ADT/STLExtras.h"
#include "llvm/CodeGen/MachineFrameInfo.h"
#include "llvm/CodeGen/MachineFunction.h"
#include "llvm/CodeGen/MachineInstrBuilder.h"
#include "llvm/CodeGen/TargetInstrInfo.h"
#include "llvm/IR/Type.h"
#include "llvm/Support/CommandLine.h"
#include "llvm/Support/ErrorHandling.h"

using namespace llvm;

#define GET_REGINFO_TARGET_DESC
#include "LC3GenRegisterInfo.inc"

LC3RegisterInfo::LC3RegisterInfo();

//===----------------------------------------------------------------------===//
// Callee Saved Register
//===----------------------------------------------------------------------===//
const MCPhysReg *
LC3RegisterInfo:: getCalleeSavedRegs(const MachineFunction *MF) const {

};

const uint32_t *
LC3RegisterInfo:: getCallPreservedMask(const MachineFunction &MF,
                                    CallingConv::ID) const override;
BitVector
LC3RegisterInfo:: getReservedRegs(const MachineFunction &MF) const override;

const
LC3RegisterInfo:: TargetRegisterClass * getPointerRegClass(const MachineFunction &MF, 
                                                unsigned Kind = 0) const override;

// "resolve a frame index operand of an instruction to reference the indicated base register plus offset instead"
void
LC3RegisterInfo:: resolveFrameIndex(MachineInstr &MI, unsiged BaseReg, int64_t Offset) const override;

// "determined weather a given base register plus offset immediate is encodable to resolve a frame index"
bool
LC3RegisterInfo:: isFrameOffsetLegal(const MachineInstr *MI, usinged BaseReg, int64_t Offset) const override;

// returns register used as a base for values allocated in the current stack frame (?)
Register
LC3RegisterInfo:: getFrameRegister(const MachineFunction &MF) const override;

void
LC3RegisterInfo:: eliminateFrameIndex(MachineBasicBlock::iterator MI, int SPAdj,
                        unsigned FIOperandNum,
                        RegScavenger *RS = nullptr) const override;